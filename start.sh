#!/bin/bash

# Fungsi untuk menampilkan pesan dan waktu yang ditambahkan
show_message() {
    echo "Menjalankan setelah $1 menit. Akan dilanjutkan pada: $(date -d "+$1 minutes" +"%H:%M:%S")"
}

# Perulangan tak terbatas
while true; do
    # Menampilkan pesan dan waktu yang ditambahkan
    
    node index.js
    show_message 10
    # Menunggu selama 10 menit
    sleep 600

    # Menjalankan node app.js
done
