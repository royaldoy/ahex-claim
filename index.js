const axios = require("axios");
const fs = require("fs");
const chalk = require("chalk");

async function authApp(login) {
  try {
    const response = await axios.post(
      "https://hexacore-tg-api.onrender.com/api/app-auth",
      login,
      {
        headers: {
          accept: "*/*",
          "accept-language": "en-US,en;q=0.9",
          "content-type": "application/json",
          priority: "u=1, i",
          "sec-ch-ua":
            '"Microsoft Edge";v="125", "Chromium";v="125", "Not.A/Brand";v="24", "Microsoft Edge WebView2";v="125"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"Windows"',
          "sec-fetch-dest": "empty",
          "sec-fetch-mode": "cors",
          "sec-fetch-site": "cross-site",
          Referer: "https://ago-wallet.hexacore.io/",
          "Referrer-Policy": "strict-origin-when-cross-origin",
        },
      }
    );

    return response.data.token;
    console.log(response.data);
  } catch (error) {
    return null;
    console.error("Error authenticating app:", error);
  }
}

async function claimTask(bearer) {
  try {
    const url = "https://hexacore-tg-api.onrender.com/api/mission-complete";
    const header = {
      accept: "*/*",
      "accept-language": "en-US,en;q=0.9",
      authorization: bearer,
      priority: "u=1, i",
      "sec-ch-ua":
        '"Microsoft Edge";v="125", "Chromium";v="125", "Not.A/Brand";v="24", "Microsoft Edge WebView2";v="125"',
      "sec-ch-ua-mobile": "?0",
      "sec-ch-ua-platform": '"Windows"',
      "sec-fetch-dest": "empty",
      "sec-fetch-mode": "cors",
      "sec-fetch-site": "cross-site",
      Referer: "https://ago-wallet.hexacore.io/",
      "Referrer-Policy": "strict-origin-when-cross-origin",
    };

    for (let is = 1; is < 9; is++) {
      const response = await axios.post(
        url,
        { missionId: is },
        { headers: header }
      );
    }

    return true;
    console.log(response.data);
  } catch (error) {
    return false;
    console.error("Error claiming task:", error);
  }
}

async function getBalance({ user_id }, bearer) {
  try {
    const response = await axios.get(
      `https://hexacore-tg-api.onrender.com/api/balance/${user_id}`,
      {
        headers: {
          accept: "*/*",
          "accept-language": "en-US,en;q=0.9",
          authorization: bearer,
          "content-type": "application/json",
          priority: "u=1, i",
          "sec-ch-ua":
            '"Microsoft Edge";v="125", "Chromium";v="125", "Not.A/Brand";v="24", "Microsoft Edge WebView2";v="125"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"Windows"',
          "sec-fetch-dest": "empty",
          "sec-fetch-mode": "cors",
          "sec-fetch-site": "cross-site",
          Referer: "https://ago-wallet.hexacore.io/",
          "Referrer-Policy": "strict-origin-when-cross-origin",
        },
      }
    );
    return response.data.balance;
    console.log(response.data);
  } catch (error) {
    return 0;
    console.error("Error fetching balance:", error);
  }
}

async function upgradeLevel(bearer) {
  try {
    const response = await axios.post(
      "https://hexacore-tg-api.onrender.com/api/upgrade-level",
      null,
      {
        headers: {
          accept: "*/*",
          "accept-language": "en-US,en;q=0.9",
          authorization: bearer,
          "content-type": "application/json",
          priority: "u=1, i",
          "sec-ch-ua":
            '"Microsoft Edge";v="125", "Chromium";v="125", "Not.A/Brand";v="24", "Microsoft Edge WebView2";v="125"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"Windows"',
          "sec-fetch-dest": "empty",
          "sec-fetch-mode": "cors",
          "sec-fetch-site": "cross-site",
          Referer: "https://ago-wallet.hexacore.io/",
          "Referrer-Policy": "strict-origin-when-cross-origin",
        },
      }
    );

    return true;
    console.log(response.data);
  } catch (error) {
    return false;
    console.error("Error upgrading level:", error);
  }
}

async function buyTaps(bearer) {
  try {
    const response = await axios.post(
      "https://hexacore-tg-api.onrender.com/api/buy-tap-passes",
      {
        name: "7_days",
      },
      {
        headers: {
          accept: "*/*",
          "accept-language": "en-US,en;q=0.9",
          authorization: bearer,
          "content-type": "application/json",
          priority: "u=1, i",
          "sec-ch-ua":
            '"Microsoft Edge";v="125", "Chromium";v="125", "Not.A/Brand";v="24", "Microsoft Edge WebView2";v="125"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"Windows"',
          "sec-fetch-dest": "empty",
          "sec-fetch-mode": "cors",
          "sec-fetch-site": "cross-site",
          Referer: "https://ago-wallet.hexacore.io/",
          "Referrer-Policy": "strict-origin-when-cross-origin",
        },
      }
    );
    return true;
    console.log(response.data);
  } catch (error) {
    return false;
    console.error("Error buying tap passes:", error);
  }
}

function cekBerhasilGagal(params) {
  if (!params) return chalk.red("Gagal!");
  return chalk.green("Berhasil!");
}

async function miningComplete(tap, bearer) {
  try {
    const response = await axios.post(
      "https://hexacore-tg-api.onrender.com/api/mining-complete",
      {
        taps: tap,
      },
      {
        headers: {
          accept: "*/*",
          "accept-language": "en-US,en;q=0.9",
          authorization: bearer,
          "content-type": "application/json",
          priority: "u=1, i",
          "sec-ch-ua":
            '"Microsoft Edge";v="125", "Chromium";v="125", "Not.A/Brand";v="24", "Microsoft Edge WebView2";v="125"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"Windows"',
          "sec-fetch-dest": "empty",
          "sec-fetch-mode": "cors",
          "sec-fetch-site": "cross-site",
          Referer: "https://ago-wallet.hexacore.io/",
          "Referrer-Policy": "strict-origin-when-cross-origin",
        },
      }
    );

    return true;
    console.log(response.data);
  } catch (error) {
    return false;
    console.error("Error completing mining:", error);
  }
}

async function availableTaps(bearer) {
  try {
    const response = await axios.get(
      "https://hexacore-tg-api.onrender.com/api/available-taps",
      {
        headers: {
          accept: "*/*",
          "accept-language": "en-US,en;q=0.9",
          authorization: bearer,
          "content-type": "application/json",
          priority: "u=1, i",
          "sec-ch-ua":
            '"Microsoft Edge";v="125", "Chromium";v="125", "Not.A/Brand";v="24", "Microsoft Edge WebView2";v="125"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"Windows"',
          "sec-fetch-dest": "empty",
          "sec-fetch-mode": "cors",
          "sec-fetch-site": "cross-site",
          Referer: "https://ago-wallet.hexacore.io/",
          "Referrer-Policy": "strict-origin-when-cross-origin",
        },
      }
    );
    return response.data.available_taps ?? 0;
    console.log();
  } catch (error) {
    return 0;
    console.error("Error fetching available taps:", error);
  }
}

async function startBot() {
  process.stdout.write("\x1Bc");
  console.log("``````````````````````````````````````````````");
  console.log("BOT BY LIAN ------------------------------- :)");
  console.log(",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,");
  const rawData = fs.readFileSync("token.json", "utf8");

  // Parse the JSON data
  const users = JSON.parse(rawData);

  for (const user of users) {
    const bearer = await authApp(user);

    if (bearer) {
      const avail = await availableTaps(bearer);
      const mining = avail ? await miningComplete(avail, bearer) : false;

      const claim = await claimTask(bearer);
      const level = await upgradeLevel(bearer);
      const taps = await buyTaps(bearer);
      const balance = await getBalance(user, bearer);

      console.log("==============================================");
      console.log(`Username : ${user.username}`);
      console.log(`Balance : ${balance}`);
      console.log(`Claim Daily : ${cekBerhasilGagal(mining)}`);
      console.log(`Clear Task : ${cekBerhasilGagal(claim)}`);
      console.log(`Level : ${cekBerhasilGagal(level)}`);
      console.log(`Taps ${avail} : ${cekBerhasilGagal(taps)}`);
      console.log("==============================================");
    } else {
      console.log(chalk.red(`${user.username} | Gagal Login!`));
    }
  }
}

startBot();
